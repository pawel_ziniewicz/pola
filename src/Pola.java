import java.util.Scanner;

public class Pola {

	static final String KWADRAT = "1";
	static final String TROJKAT = "2";
	static final String OKRAG = "3";
	static final String END = "end";

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner scanner=new Scanner(System.in);
		while (true){
			informacja();
			String input=scanner.next();
			if (input.equals(END)){
				break;
			}
			opcje(scanner, input);
		}
//


		scanner.close();

	}



	private static void opcje(Scanner scanner, String input) {
		switch (input) {
		case KWADRAT:
			poleKwadratu(scanner);
			break;
		case TROJKAT:
			poleTrojkata(scanner);
			break;
		case OKRAG:
			poleOkregu(scanner);
			break;
		default:
			System.out.println("Nic nie wybrales!");
		}

	}



	private static void informacja() {
		System.out.println("Program oblicz pole powierzchni figur paskich." + "\nMasz do wyboru:" + "\n" + KWADRAT
		+ "-pole kwadratu" + "\n" + TROJKAT + "-pole trójkąta" + "\n" + OKRAG + "-pole okrgu" + "\n" + END
		+ "-wyjście z programu.");

	}

	private static void poleOkregu(Scanner scanner) {
		System.out.println("Podaj promien okregu:");
		double promien=scanner.nextDouble();
		double pole=promien*promien*Math.PI;
		System.out.println("Pole okregu o promieniu="+promien+" wynosi A="+pole);

	}

	private static void poleTrojkata(Scanner scanner) {
		System.out.println("Podaj dlugosc podstawy trojkata:");
		double podstawa=scanner.nextDouble();
		System.out.println("Podaj wysokosc trojkata:");
		double wysokosc=scanner.nextDouble();
		double pole=podstawa*wysokosc*0.5;
		System.out.println("Pole trojkata o podstawie a="+podstawa+" i wysokosci h="+wysokosc+ "wynosi A="+pole);

	}

	private static void poleKwadratu(Scanner scanner) {
		System.out.println("Podaj dlugosc boku kwadratu:");
		double bok=scanner.nextDouble();
		double pole=bok*bok;
		System.out.println("Pole kwadratu o boku="+bok+" wynosi A="+pole);

	}

}
